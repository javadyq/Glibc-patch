# Glibc-Patch Check Notes

In order to ensure the security and integrity, it is recommended for users to compare hash number of download package with the values we provide as below.

## Check Values

Filename: glibc-2.17\glibc-2.17-patch-1910.tar.gz

MD5     : FE6235B0117BD5C872563598618A2EF6

SHA1    : 25FF7EC2B6E3D4934EEF5D29FE5DE1125FE608E5

SHA256  : 7C720E4E1B8B289B9FF4C590EF14F73110E5D946B04679B82E817A034D102572



Filename: glibc-2.29\ glibc-2.29-patch-1910.tar.gz

MD5     : D4E6588B1C594553C116017D5D233B50

SHA1    : D942121C40A4200EA4CF30ECE34D90175676D0FB

SHA256  : 41C4AFB9F050C4D1BA32A60F996D8209EEE552024CFC65C2251BB91371B9594D



## Check Command Help

### Windows

certutil -hashfile [filename] [Check Type]

​	-Check Type		MD5/SHA1/SHA256

### Linux

[Check Type]  [filename]

​	-Check Tpye		md5sum/sha1sum/sha256sum